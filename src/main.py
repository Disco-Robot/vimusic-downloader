import os
import requests
import eyed3
import sqlite3
import argparse
import signal
from yt_dlp import YoutubeDL as ydl


def downloadMusic(url, output_folder):

    output_folder = os.path.expanduser(os.path.expandvars(output_folder))

    ydl_options = {
        'format': 'bestaudio/best',
        'outtmpl': f'{output_folder}/%(title)s - %(artist)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3'
        }],
    }

    with ydl(ydl_options) as video:

        info_dict = video.extract_info(url, download=True)
        
        title = info_dict['title']
        artist = info_dict.get('artist', 'Unknown Artist')
        album = info_dict.get('album', 'Unknown Album')
        thumbnails = info_dict.get('thumbnails', [])
        
        album_cover_url = next((thumb.get('url') for thumb in thumbnails if thumb.get('width', 0) > 300 and thumb.get('height', 0) > 300), thumbnails[0].get('url', ''))

        filepath = f'{output_folder}/{title} - {artist}.mp3'

        print(f"File downloaded: {filepath}")

        try:
            audiofile = eyed3.load(filepath)
            if audiofile is not None:


                audiofile.tag.artist = artist
                audiofile.tag.album = album
                audiofile.tag.title = title

                audiofile.tag.comments.set(url)
                
                if album_cover_url:
                    thumbnail_data = requests.get(album_cover_url).content
                    audiofile.tag.images.set(3, thumbnail_data, 'image/jpeg', u'Album Cover')

                audiofile.tag.save()
                print(f"Tags embedded successfully in {filepath}")
            else:
                print(f"Failed to load {filepath} as an MP3 file.")
        except Exception as e:
            print(f"An error occurred while loading {filepath}: {e}")


def downloadSet(idx_array, output_folder) :

    failed_songs = []

    for idx in idx_array :
        try :
            url = "https://music.youtube.com/watch?v=" + idx 
            downloadMusic(url, output_folder)
        except :
            print( "Could not download " + "https://music.youtube.com/watch?v=" + idx)
            failed_songs.append(idx)
            continue
    
    return failed_songs

def getFavoriteSet(viMusic_backup_path):
    
    conn = sqlite3.connect(os.path.expanduser(os.path.expandvars(viMusic_backup_path)))

    cursor = conn.cursor()
    cursor.execute("SELECT id FROM Song WHERE likedAt NOT NULL;")

    results = { item[0] for item in cursor.fetchall() }

    conn.close()

    return results

def getPlaylistSet(viMusic_backup_path,playlist_id):
    
    conn = sqlite3.connect(os.path.expanduser(os.path.expandvars(viMusic_backup_path)))

    cursor = conn.cursor()
    cursor.execute(f"SELECT songId FROM SongPlaylistMap WHERE playlistId = {playlist_id};")
    
    results = { item[0] for item in cursor.fetchall() }

    conn.close()

    return results


def getPlaylist(viMusic_backup_path):

    conn = sqlite3.connect(os.path.expanduser(os.path.expandvars(viMusic_backup_path)))

    cursor = conn.cursor()
    cursor.execute("SELECT id,name FROM Playlist;")

    results = dict()

    for item in cursor.fetchall() :
        results[item[0]] = item[1]

    conn.close()

    while True :
  
        print("Choose an option:")

        for id in results.keys() :
            print(str(id) + " - " + results[id])

        option = input(": ")

        if int(option) in results.keys() :
            break
        

    return option


def getIdSetFromFolder(output_folder):
    idSet = set()

    for filename in os.listdir(output_folder):

        
        if filename.endswith(".mp3"):
            mp3_file = os.path.join(output_folder, filename)
            try:
                audiofile = eyed3.load(mp3_file)
                idSet.add(audiofile.tag.comments[0].text[34:])
            except Exception as error:
                print(f"Error processing {filename}: {error}")
                continue

    return idSet


def removeSetFromFolder(output_folder,selected_set):

    for filename in os.listdir(output_folder):

        
        if filename.endswith(".mp3"):
            mp3_file = os.path.join(output_folder, filename)
            try:
                audiofile = eyed3.load(mp3_file)
                if audiofile.tag.comments[0].text[34:] in selected_set :
                    os.remove(mp3_file)
            except Exception as error:
                print(f"Error processing {filename}: {error}")
                continue

def getDiff(originalSet,newSet):
    removedElements = originalSet - newSet
    addElements = newSet - originalSet
    
    return [addElements,removedElements]

def downloadFavorites(viMusic_backup_path,output_folder,diff=False):
    if not diff :
        downloadSet(getFavoriteSet(viMusic_backup_path),output_folder)
    else :
        addedElements,removedElements = getDiff(getIdSetFromFolder(output_folder),getFavoriteSet(viMusic_backup_path))
        removeSetFromFolder(output_folder,removedElements)
        downloadSet(addedElements,output_folder)

def downloadPlaylist(viMusic_backup_path,output_folder,diff=False) :
    if not diff :
        downloadSet(getPlaylistSet(viMusic_backup_path,getPlaylist(viMusic_backup_path)),output_folder)
    else :
        addedElements,removedElements = getDiff(getIdSetFromFolder(output_folder),getPlaylistSet(viMusic_backup_path,getPlaylist(viMusic_backup_path)))
        removeSetFromFolder(output_folder,removedElements)
        downloadSet(addedElements,output_folder)

def termSigHandler(signum,frame) :
    print("\nStopping . . .")
    quit()


if __name__ == "__main__":

    signal.signal(signal.SIGINT,termSigHandler)
    
    parser = argparse.ArgumentParser(prog='ViMusicDownloader',description='ViMusic playlist downloader')
    parser.add_argument('-o','--output',required=True)
    parser.add_argument('-b','--backup',required=True)
    parser.add_argument('-p','--playlist',action='store_true')
    parser.add_argument('-d','--diff',action='store_true')

    args = parser.parse_args()

    backupfile = args.backup
    output_folder = args.output
    diff = args.diff

    if args.playlist :
        downloadPlaylist(backupfile,output_folder,diff)
    else :
        downloadFavorites(backupfile,output_folder,diff)
